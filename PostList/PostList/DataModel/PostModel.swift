//
//  PostModel.swift
//  PostList
//
//  Created by TrungPhan on 8/23/17.
//
//

import Foundation
class PostModel:NSObject, NSCoding  {
    var id: String
    var createdDate:Date
    var title:String
    var detail: String
    var byUser: String
    var lastEditedBy: String?
    var votes = 0
    
    init(id:String, createdDate:Date, title:String, detail:String, byUser:String, lastEditedBy:String?, votes:Int) {
        self.id = id
        self.createdDate = createdDate
        self.title = title
        self.detail = detail
        self.byUser = byUser
        self.lastEditedBy = lastEditedBy
        self.votes = votes
    }
    required convenience init(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeObject(forKey: "id") as!String
        let createdDate = aDecoder.decodeObject(forKey: "createdDate") as! Date
        let title = aDecoder.decodeObject(forKey: "title") as! String
        let detail = aDecoder.decodeObject(forKey: "detail") as! String
        let byUser = aDecoder.decodeObject(forKey: "byUser") as! String
        let lastEditedBy = aDecoder.decodeObject(forKey: "lastEditedBy") as? String
        let votes = aDecoder.decodeInteger(forKey: "votes")

        self.init(id: id, createdDate: createdDate, title: title, detail: detail, byUser: byUser, lastEditedBy: lastEditedBy, votes: votes)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(createdDate, forKey: "createdDate")
        aCoder.encode(title, forKey: "title")
        aCoder.encode(detail, forKey: "detail")
        aCoder.encode(byUser, forKey: "byUser")
        aCoder.encode(lastEditedBy, forKey: "lastEditedBy")
        aCoder.encode(votes, forKey: "votes")

    }
}
extension PostModel {
    convenience init(title: String, detail:String, byUser: String) {
        self.init(id: UUID().uuidString, createdDate: Date(), title: title, detail: detail, byUser: byUser, lastEditedBy: "", votes: 0)
    }
}
