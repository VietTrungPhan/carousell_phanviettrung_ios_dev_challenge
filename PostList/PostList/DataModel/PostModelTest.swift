//
//  PostModelTest.swift
//  PostList
//
//  Created by TrungPhan on 8/23/17.
//
//

import XCTest
@testable import PostList

class PostModelTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testCreateModel() {
        let  postModel = PostModel(title: "title", detail: "detail", byUser: "Nick")
        XCTAssertNotNil(postModel)
        XCTAssertEqual(postModel.title, "title")
        XCTAssertEqual(postModel.detail, "detail")
        XCTAssertEqual(postModel.byUser, "Nick")
    }
}
