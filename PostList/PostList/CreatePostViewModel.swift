//
//  CreatePostViewModel.swift
//  PostList
//
//  Created by TrungPhan on 8/28/17.
//
//

import Foundation

class CreatePostViewModel {
    func createPost(title: String, detail: String) -> Bool {
        
        //random user
        let users = ["Nick", "Ann", "Willow", "James", "Daniel", "Tom"]
        let postUser = users[Int(arc4random() % UInt32(users.count))]
        
        let post = PostModel(title: title, detail: detail, byUser: postUser)
        return DataManager.sharedInstance.addnewPost(post: post)
    }
}

