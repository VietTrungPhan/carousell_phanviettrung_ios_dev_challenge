//
//  File.swift
//  PostList
//
//  Created by TrungPhan on 8/28/17.
//
//

import Foundation
import UIKit

extension UIView {
    func border(width:CGFloat, color: UIColor, radius: CGFloat?) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
        if let radius = radius {
            self.layer.cornerRadius = radius
            self.layer.masksToBounds = true
        }
    }
    func border(width:CGFloat, color: UIColor) {
        self.border(width: width, color: color, radius: nil)
    }
}
