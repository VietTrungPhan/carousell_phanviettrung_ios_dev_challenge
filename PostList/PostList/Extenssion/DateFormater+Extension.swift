//
//  DateFormater+Extension.swift
//  PostList
//
//  Created by TrungPhan on 8/28/17.
//
//

import Foundation
extension DateFormatter {
    class func dateFormaterStyleMMDDYY() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/YY"
        return dateFormatter
    }
}
