//
//  PostDetailViewModel.swift
//  PostList
//
//  Created by TrungPhan on 8/28/17.
//
//

import Foundation
import RxDataSources
import RxSwift

class PostDetailViewModel {
    
    var dataSource: Variable<[PostDetailSection]> = Variable([])
    var postModel: PostModel? {
        didSet {
            guard let postModel = postModel else {
                return
            }
           dataSource.value = createSection(postModel:postModel)
        }
    }
    
    /**
     createSection genarate tableview section from post model
     
     - parameter postModel: PostModel.
     
     - returns: Array of PostDetailSection.
     */
    func createSection(postModel:PostModel) -> [PostDetailSection] {
        return [PostDetailSection.Default(items: [PostDetailCell.HeaderCellItem(postModel), PostDetailCell.BodyCellItem(postModel)])]
    }
    
    /**
     upVote is function use to increase post vote, save to database and reindex data
     It also fake user Edit post
     - parameter postModel: PostModel use to change.
     */
    func upVote(postModel: PostModel) {
        postModel.votes += 1
        let users = ["Nick", "Ann", "Willow", "James", "Daniel", "Tom"]
        let editedUser = users[Int(arc4random() % UInt32(users.count))]
        postModel.lastEditedBy = editedUser
        if DataManager.sharedInstance.updatePost(post: postModel) {
            dataSource.value = createSection(postModel:postModel)
        }
    }
    func deVote(postModel:PostModel) {
        postModel.votes -= 1
        let users = ["Nick", "Ann", "Willow", "James", "Daniel", "Tom"]
        let editedUser = users[Int(arc4random() % UInt32(users.count))]
        postModel.lastEditedBy = editedUser

        if DataManager.sharedInstance.updatePost(post: postModel) {
            dataSource.value = createSection(postModel:postModel)
        }

    }
}

enum PostDetailCell {
    case HeaderCellItem(PostModel)
    case BodyCellItem(PostModel)
}

enum PostDetailSection {
    typealias Item = PostDetailCell
    case Default(items: [Item])
}

extension PostDetailSection : SectionModelType{
    
    var items: [Item] {
        switch  self {
        case . Default  (items: let items):
            return items.map {$0}
        }
    }
    init(original: PostDetailSection, items: [Item]) {
        switch original {
        case let .Default      (items:items):
            self = .Default    (items:items)
        }
    }
}
