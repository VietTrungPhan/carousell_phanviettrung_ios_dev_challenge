//
//  HomeViewController.swift
//  PostList
//
//  Created by TrungPhan on 8/28/17.
//
//

import UIKit
import RxDataSources
import RxSwift
import RxCocoa

class HomeViewController: UIViewController {
    
    @IBOutlet weak var btnFilter: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    fileprivate var disposeBag = DisposeBag()
    fileprivate let viewModel = HomeViewModel()
    private let dataSource = RxTableViewSectionedReloadDataSource<HomeSection>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        self.title = "Carousell Overflow"
        setupTableView()
        setupFilter()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //reload data each time view appear
        viewModel.dataSource.value = viewModel.fetchData(isAccsending: viewModel.isFilterAccsending.value, numberOfItems: 20)
    }
    
    //Setup table view helper, use to map viewModel datasource to tableview UI
    private func setupTableView() {
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        dataSource.configureCell = {(section, tv, indexPath, cellType) in
            switch cellType {
            case .EmptyCellItem():
                let cell = tv.dequeueReusableCell(withIdentifier: "HomeEmptyTableViewCell") as! HomeEmptyTableViewCell
                return cell
            case .PostCellItem(let postModel):
                let cell = tv.dequeueReusableCell(withIdentifier: "HomePostTableViewCell") as! HomePostTableViewCell
                cell.postModel = postModel
                cell.btnViewDetail.rx.tap
                    .throttle(1.0, scheduler: MainScheduler.instance)
                    .subscribe(onNext: { [weak self] _ in
                        guard let strongSelf = self else {return}
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
                        vc.postModel = postModel
                        strongSelf.navigationController?.pushViewController(vc, animated: true)
                    })
                    .addDisposableTo(cell.disposeBag)
                return cell
            }
        }
        bindTableView()
    }
    private func bindTableView() {
        viewModel.dataSource.asObservable().bind(to: tableView.rx.items(dataSource:self.dataSource)).addDisposableTo(self.disposeBag)
    }
}
extension HomeViewController {
    /**
     setupFilter is function use to track user tap on Filter Buton then triger view model reload posts by filter condition 
     .It also control button filter state enable-disabe if we don't have post(s) to filter
     */
    fileprivate func setupFilter()
    {
        btnFilter.rx.tap
            .throttle(1.0, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                guard let strongSelf = self else {return}
                strongSelf.viewModel.isFilterAccsending.value = !strongSelf.viewModel.isFilterAccsending.value
            })
            .addDisposableTo(disposeBag)
        viewModel.observerFilterChanged()
        
        //If section item is not kind of EmptySection => can filter || disable filter button
        viewModel.dataSource.asObservable().observeOn(MainScheduler.instance).subscribe(onNext:{[weak self] sections in
            guard let strongSelf = self else {return}
            let isNotEnabled = sections.filter{ section in
                switch section {
                case .Empty( _):
                    return true
                default:
                    return false
                }
            }.count == 1
            strongSelf.btnFilter.isEnabled = !isNotEnabled
        }).addDisposableTo(disposeBag)
    }
    
}

