//
//  HomePostTableViewCell.swift
//  PostList
//
//  Created by TrungPhan on 8/28/17.
//
//

import UIKit
import RxSwift

class HomePostTableViewCell: UITableViewCell {

    @IBOutlet weak var lblPostBy: UILabel!
    @IBOutlet weak var lblNumberOfVote: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    
    @IBOutlet weak var btnViewDetail: UIButton!
    
    var disposeBag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    var postModel:PostModel? {
        didSet {
            guard let postModel = postModel else{return}
            lblTitle.text = postModel.title
            lblNumberOfVote.text = "\(postModel.votes)"
            lblCreatedDate.text = DateFormatter.dateFormaterStyleMMDDYY().string(from: postModel.createdDate)
            lblPostBy.attributedText = "by: ".append(string: postModel.byUser, color: #colorLiteral(red: 1, green: 0.5848525763, blue: 0.003539679805, alpha: 1), fontSize: lblPostBy.font.pointSize)
        }
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }

}
