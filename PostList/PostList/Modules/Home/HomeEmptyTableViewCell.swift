//
//  HomeEmptyTableViewCell.swift
//  PostList
//
//  Created by TrungPhan on 8/28/17.
//
//

import UIKit
class HomeEmptyTableViewCell: UITableViewCell {
    @IBOutlet weak var btnAskNewQuestion: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnAskNewQuestion.border(width: 1, color: #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1), radius: 3.0)
    }
}
