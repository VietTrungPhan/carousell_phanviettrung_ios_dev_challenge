//
//  HomeViewModel.swift
//  PostList
//
//  Created by TrungPhan on 8/23/17.
//
//

import Foundation
import RxDataSources
import RxSwift


class HomeViewModel {
    var isFilterAccsending: Variable<Bool> = Variable(true)
    var dataSource:Variable<[HomeSection]> = Variable([])
    var disposeBag = DisposeBag()
    /**
     fetchData get list of post by sort condition and number of items
     
     - parameter isAccsending: by votes count sort condition.
     - parameter numberOfItems: limit number of items want to get.
     
     - returns: [HomeSection] : Array of section model map to UI.
     */

    func fetchData(isAccsending: Bool, numberOfItems: Int) -> [HomeSection] {
        
        let postModels = DataManager.sharedInstance.getTopPosts(isAsscending: isAccsending, numberOfItems: numberOfItems)
        if let postModels = postModels {
            return [HomeSection.Default(items:postModels.map{postModel in
                return HomeCell.PostCellItem(postModel)
            })]
        }else {
            return [HomeSection.Empty(items: [HomeCell.EmptyCellItem()])]
        }
    }
    
    /**
     observerFilterChanged is function watch  sort condition changed and reload data source for table view
     */
    func observerFilterChanged() {
        isFilterAccsending.asObservable().subscribe(onNext:{[weak self] isFilterAccsending in
            guard let strongSelf = self else {return}
            strongSelf.dataSource.value = strongSelf.fetchData(isAccsending: isFilterAccsending, numberOfItems: 20)
        }).addDisposableTo(disposeBag)
    }
}


enum HomeCell {
    case PostCellItem(PostModel)
    case EmptyCellItem()
}

enum HomeSection {
    typealias Item = HomeCell
    case Default(items: [Item])
    case Empty(items:[Item])
}

extension HomeSection : SectionModelType{
    
    var items: [Item] {
        switch  self {
        case . Default  (items: let items):
            return items.map {$0}
        case .Empty(items: let items):
            return items.map {$0}
        }
    }
    init(original: HomeSection, items: [Item]) {
        switch original {
        case let .Default      (items:items):
            self = .Default    (items:items)
        case let .Empty      (items:items):
            self = .Empty    (items:items)
        }
    }
}


