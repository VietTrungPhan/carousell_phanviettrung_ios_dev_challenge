//
//  CreatePostViewController.swift
//  PostList
//
//  Created by TrungPhan on 8/28/17.
//
//

import UIKit
import RxCocoa
import RxSwift
import RxKeyboard
import RxGesture

class CreatePostViewController: UIViewController {
    @IBOutlet weak var txtFieldTitle: UITextField!
    @IBOutlet weak var txtViewDetail: UITextView!
    @IBOutlet weak var layoutConstraintBottom: NSLayoutConstraint!
    
    fileprivate var disposeBag = DisposeBag()
    fileprivate let viewModel = CreatePostViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Create New Post"
        handleKeyboardShowHide()
        dismissKeyboardIfNeeded()
        setupTitle()
        setUpBarBtn()
        limited255Chars()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        txtFieldTitle.becomeFirstResponder()
    }
    private func limited255Chars() {
        txtFieldTitle.rx.text.subscribe(onNext:{[weak self] text in
            guard let strongSelf = self, let text = text else {return}
                if text.characters.count > 255 {
                    strongSelf.txtFieldTitle.text = text.substring(to: text.index(text.startIndex, offsetBy: 254))
                }
        }).addDisposableTo(disposeBag)
        txtViewDetail.rx.text.subscribe(onNext:{[weak self] text in
            guard let strongSelf = self,let text = text else {return}
                if text.characters.count > 255 {
                    strongSelf.txtViewDetail.text = text.substring(to: text.index(text.startIndex, offsetBy: 254))
                }
        }).addDisposableTo(disposeBag)
        
    }
}

//Setup bar Btn
extension CreatePostViewController {
    func setUpBarBtn(){
        let btnCreate  = UIBarButtonItem(title: "Create", style: .plain, target: nil, action: nil)
        navigationItem.rightBarButtonItem = btnCreate
        btnCreate.rx.tap
            .throttle(1.0, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                guard let strongSelf = self else {return}
                let isSuccess = strongSelf.viewModel.createPost(title: strongSelf.txtFieldTitle.text!, detail: strongSelf.txtViewDetail.text)
                if !isSuccess {
                    let alert = UIAlertController(title: "Error create post", message: "Something wrong happened. Please try again later!", preferredStyle: .alert)
                    let noAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                    alert.addAction(noAction)
                    strongSelf.present(alert, animated: true, completion: nil)
                }else {
                    strongSelf.dismiss(animated: true, completion: nil)
                }
            })
            .addDisposableTo(disposeBag)
        
        let btnDismissViewController = UIBarButtonItem(image: #imageLiteral(resourceName: "ico_close"), style: .plain, target: nil, action: nil)
        btnDismissViewController.rx.tap
            .throttle(1.0, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                guard let strongSelf = self else {return}
                if let title =  strongSelf.txtFieldTitle.text,title.characters.count > 0 || strongSelf.txtViewDetail.text.characters.count > 0 {
                    let alert = UIAlertController(title: "Cancel post", message: "Are you sure to cancel post?", preferredStyle: .alert)
                    let noAction = UIAlertAction(title: "Continue Post", style: .cancel, handler: nil)
                    let cancelAction = UIAlertAction(title: "Cancel Anyway", style: .default, handler: {[weak self] _ in
                        guard let strongSelf = self else {return}
                        strongSelf.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(noAction)
                    alert.addAction(cancelAction)
                    strongSelf.present(alert, animated: true, completion: nil)
                }else {
                    strongSelf.dismiss(animated: true, completion: nil)
                }

            })
            .addDisposableTo(disposeBag)
        navigationItem.leftBarButtonItem = btnDismissViewController
        
        //Enable - Disable btn create
        Observable.combineLatest(txtViewDetail.rx.text, txtFieldTitle.rx.text)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { (detail, title) in
                if  let detail = detail, let title = title {
                    btnCreate.isEnabled = detail.characters.count > 0 && title.characters.count > 0
                }else {
                     btnCreate.isEnabled = false
                }

        }).addDisposableTo(disposeBag)
    }
}

//Setup title
extension CreatePostViewController {
    fileprivate func setupTitle(){
        txtFieldTitle.border(width: 1, color: #colorLiteral(red: 0.4139623153, green: 0.2897135417, blue: 1, alpha: 1), radius: 3.0)
        txtFieldTitle.leftViewMode = .always
        txtFieldTitle.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 40))
        txtViewDetail.border(width: 1, color: #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1), radius: 3.0)
        txtFieldTitle.returnKeyType = .next
        txtFieldTitle.rx.controlEvent(.editingDidEndOnExit)
            .subscribe(onNext:{[weak self] _ in
                guard let strongSelf = self else {return}
                strongSelf.txtViewDetail.becomeFirstResponder()
            }).addDisposableTo(disposeBag)
    }
}

//Handle keyboard
extension CreatePostViewController {
    fileprivate func handleKeyboardShowHide() {
        RxKeyboard.instance.visibleHeight
            .drive(onNext: {[weak self] keyboardVisibleHeight in
                
                guard let strongSelf = self else {return}
                strongSelf.layoutConstraintBottom.constant = keyboardVisibleHeight == 0 ? 16:keyboardVisibleHeight+16
                UIView.animate(withDuration: 0, animations: {
                    strongSelf.view.layoutIfNeeded()
                })
            })
            .disposed(by: disposeBag)
    }
    fileprivate func dismissKeyboardIfNeeded() {
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tapGesture)
    }
    func dismissKeyboard(){
        UIApplication.shared.sendAction(#selector(self.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
