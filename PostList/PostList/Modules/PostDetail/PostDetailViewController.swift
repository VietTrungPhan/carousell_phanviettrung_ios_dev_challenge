//
//  PostDetailViewController.swift
//  PostList
//
//  Created by TrungPhan on 8/28/17.
//
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class PostDetailViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var viewModel: PostDetailViewModel?
    var disposeBag = DisposeBag()
    let dataSource = RxTableViewSectionedReloadDataSource<PostDetailSection>()

    var postModel:PostModel? {
        didSet {
            guard let postModel = postModel else {
                return
            }
            title = postModel.title
            viewModel = PostDetailViewModel()
            viewModel?.postModel = postModel
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        setupTableView()
        bindTableView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func setupTableView() {
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        dataSource.configureCell = { (section, tv, indexPath, cellType) in
            switch cellType {
            case .HeaderCellItem(let postModel):
                let cell = tv.dequeueReusableCell(withIdentifier: "PostDetailHeaderTableViewCell") as! PostDetailHeaderTableViewCell
                cell.postModel = postModel
                cell.btnUpVote.rx.tap
                    .throttle(1.0, scheduler: MainScheduler.instance)
                    .subscribe(onNext: { [weak self] _ in
                        guard let strongSelf = self else {return}
                        strongSelf.viewModel?.upVote(postModel: postModel)
                    })
                    .addDisposableTo(cell.disposeBag)
                cell.btnDevote.rx.tap
                    .throttle(1.0, scheduler: MainScheduler.instance)
                    .subscribe(onNext: { [weak self] _ in
                        guard let strongSelf = self else {return}
                        strongSelf.viewModel?.deVote(postModel: postModel)
                    })
                    .addDisposableTo(cell.disposeBag)
                return cell
            case .BodyCellItem(let postModel):
                let cell = tv.dequeueReusableCell(withIdentifier: "PostDetailBodyTableViewCell") as! PostDetailBodyTableViewCell
                cell.postModel = postModel
                return cell
            }
        }
    }
    func bindTableView() {
        viewModel!.dataSource.asObservable().bind(to: self.tableView.rx.items(dataSource:self.dataSource)).addDisposableTo(self.disposeBag)
    }

    
}
