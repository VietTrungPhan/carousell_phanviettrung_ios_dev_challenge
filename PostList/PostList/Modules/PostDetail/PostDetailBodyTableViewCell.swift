//
//  PostDetailBodyTableViewCell.swift
//  PostList
//
//  Created by TrungPhan on 8/28/17.
//
//

import UIKit

class PostDetailBodyTableViewCell: UITableViewCell {

    @IBOutlet weak var lblBody: UILabel!
    @IBOutlet weak var lblUserAskName: UILabel!
    @IBOutlet weak var lblUserEditName: UILabel!
    
    @IBOutlet weak var bronView1: UIView!
    @IBOutlet weak var goldView1: UIView!
    @IBOutlet weak var bronView2: UIView!
    @IBOutlet weak var goldView2: UIView!
    
    @IBOutlet weak var lblLastEditBy: UILabel!
    @IBOutlet weak var userGroupView1: UIView!
    @IBOutlet weak var userGroupView2: UIView!
    
    var postModel:PostModel? {
        didSet {
            guard let postModel = postModel else {return}
            lblBody.text = postModel.detail
            lblUserAskName.text = postModel.byUser
            
            if let lastEditedBy = postModel.lastEditedBy {
                lblUserEditName.text = lastEditedBy
                userGroupView2.isHidden = false
                lblLastEditBy.isHidden = false
            }else {
                userGroupView2.isHidden = true
                lblLastEditBy.isHidden = true
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bronView1.border(width: 0, color: .clear, radius: 4)
        bronView2.border(width: 0, color: .clear, radius: 4)
        goldView1.border(width: 0, color: .clear, radius: 4)
        goldView1.border(width: 0, color: .clear, radius: 4)
        userGroupView1.border(width: 0, color: .clear, radius: 4)
        userGroupView2.border(width: 0, color: .clear, radius: 4)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
