//
//  PostDetailHeaderTableViewCell.swift
//  PostList
//
//  Created by TrungPhan on 8/28/17.
//
//

import UIKit
import RxSwift
class PostDetailHeaderTableViewCell: UITableViewCell {

    var disposeBag = DisposeBag()
    
    @IBOutlet weak var btnUpVote: UIButton!
    
    @IBOutlet weak var btnDevote: UIButton!
    
    @IBOutlet weak var lblNumberOfVote: UILabel!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    var postModel: PostModel? {
        didSet {
            guard let postModel = postModel else {
                return
            }
            lblTitle.text = postModel.title
            lblNumberOfVote.text = "\(postModel.votes)"
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
}
