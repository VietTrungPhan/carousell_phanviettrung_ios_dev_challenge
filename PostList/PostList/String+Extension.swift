//
//  String+Extenssion.swift
//  PostList
//
//  Created by TrungPhan on 8/28/17.
//
//

import Foundation
import UIKit
extension String{
    func append(string:String, color:UIColor, fontSize:CGFloat) -> NSMutableAttributedString{
        return NSMutableAttributedString(string: self).appEndColor(text: string, color: color, fontSize: fontSize)
    }

}
