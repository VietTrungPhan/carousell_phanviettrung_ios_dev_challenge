//
//  DataManager.swift
//  PostList
//
//  Created by TrungPhan on 8/25/17.
//
//
/// .DataManager is Singleton class use to manage Insert, Delete, Update, Index data of app
import Foundation
class DataManager {
    static let sharedInstance = DataManager()
    let database = DataBase<PostModel>()
    
    /**
     addNewPost is func use to add new post in database and indexed position of post for optimazation Read, Write, Update later
     
     - parameter post: PostModel.
     - returns: isSuccess? true if Insert and Indexed success.
     */
    func addnewPost(post: PostModel) -> Bool {
        if database.Insert(object: post, byKey: post.votes) {
          if let inBlockIndex =  database.Select(byKey: post.votes)?.Index(condition: { $0.id == post.id}) {
            return database.insertDataBaseIndex(object: DataBaseIndex(objectId: post.id, blockDataIndex: post.votes, inBlockIndex: inBlockIndex))}
        }
        return false
    }
    
    /**
     updatePost is function use to update post
     
     - parameter post: post need Update.
     - returns: isSuccess.
     */
    func updatePost(post:PostModel) -> Bool {
       let indexed = database.getDataBaseIndex(byId: post.id)
        
        //Can store same block => Same index, don't need reindex
        if indexed?.blockDataIndex == post.votes {
            if let blockData = database.Select(byKey: (indexed?.blockDataIndex)!) {
                let isSuccess = blockData.Update(object: post, atIndex: (indexed?.blockDataIndex)!)
                return isSuccess
            }
        }else {
            // Delete old Data in old Block and Add new
            if database.removeDataBaseIndex(byId: post.id) {
                //remove indexed too
               let _ = database.removeDataBaseIndex(byId: post.id)
            }
            return addnewPost(post: post)
        }
        return false
    }
    
    /**
     getpost is function use to get posts in database by giving condition
     
     - parameter isAsscending: Bool sorted condition.
     - parameter numberOfItems: maximum number post can take.
     
     - returns: Array of Post.
     */
    func getTopPosts(isAsscending: Bool, numberOfItems: Int)->[PostModel]? {
        var votes = database.getDataBaseIndexedSortedByVoted()
        if votes.count == 0 {
            return nil
        }
        if isAsscending {
            votes = votes.reversed()
        }
        return votes.prefix(numberOfItems).map{indexed in
            return (database.Select(byKey: indexed.blockDataIndex!)?.Select(atIndex: indexed.inBlockIndex!))!
        }
    }
}

