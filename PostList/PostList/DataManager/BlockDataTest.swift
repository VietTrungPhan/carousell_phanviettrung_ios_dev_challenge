//
//  BlockDataTest.swift
//  PostList
//
//  Created by TrungPhan on 8/23/17.
//
//

import XCTest
@testable import PostList
class BlockDataTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testInitBlock() {
        let block = BlockData<Int>(items: [1,2,3])
        XCTAssertNotNil(block)
    }
    
    func testSelectSuccess(){
        let block = BlockData<Int>(items: [1,2,3])
        XCTAssertNotNil(block.Select(atIndex:0))
        XCTAssertEqual(block.Select(atIndex:0)!, 1)
    }
    func testSelectFFailue(){
        let block = BlockData<Int>(items: [1,2,3])
        XCTAssertNil(block.Select(atIndex: 3))
    }
    func testInsert() {
        let block = BlockData<Int>(items: [0])
        XCTAssertTrue(block.Insert(object: 1))
        XCTAssertEqual(block.Select(atIndex: 1), 1)
    }
    func testSelectAll() {
        let block = BlockData<Int>(items: [1,2,3])
        XCTAssertEqual(block.SelectAll()!, [1,2,3])
    }
    func testUpdateSuccess() {
        let block = BlockData<Int>(items: [1,2,3])
        XCTAssertEqual(block.Select(atIndex: 2), 3)
        XCTAssertTrue(block.Update(object: 5, atIndex: 2))
        XCTAssertEqual(block.Select(atIndex: 2), 5)

    }
    func testUpdateFailue() {
        let block = BlockData<Int>(items: [1,2,3])
        XCTAssertEqual(block.Select(atIndex: 2), 3)
        XCTAssertFalse(block.Update(object: 5, atIndex: 3))
    }
    func testAddTwoBlockData() {
        let lhsBlockData = BlockData<Int>(items: [1,2,3])
        let rhsBlockData = BlockData<Int>(items: [4,5,6])
        
        let addBlock = lhsBlockData + rhsBlockData
        XCTAssertEqual(addBlock.SelectAll()! , [1,2,3,4,5,6])

    }
    func testFindObjectByGivingCondition() {
        struct Human {
            var name: String
            var age: Int
        }
        let Nick = Human(name:"Nick", age: 27)
        let Ann = Human(name:"Ann", age: 10)
        let Willow = Human(name:"Willow", age: 5)
        let blockData = BlockData(items:[Nick, Ann, Willow])
        
        let resultNick = blockData.Find(byCondition: {$0.name == "Nick"}).first
        XCTAssertEqual(resultNick?.name, Nick.name)
        
        let resultChildren = blockData.Find(byCondition: {$0.age <= 10})
        XCTAssertEqual(resultChildren.count, 2)
    }
    func testGetIndex() {
        struct Human {
            var name: String
            var age: Int
        }
        let Nick = Human(name:"Nick", age: 27)
        let Ann = Human(name:"Ann", age: 10)
        let Willow = Human(name:"Willow", age: 5)
        let blockData = BlockData(items:[Nick, Ann, Willow])

        XCTAssertEqual( blockData.Index{ (human) -> Bool in
            human.name == "Ann"
        }, 1)
        
    }
}
