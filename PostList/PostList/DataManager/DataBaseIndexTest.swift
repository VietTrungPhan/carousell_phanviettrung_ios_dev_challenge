//
//  DataBaseIndexTest.swift
//  PostList
//
//  Created by TrungPhan on 8/24/17.
//
//

import XCTest
@testable import PostList
class DataBaseIndexTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testInitObject() {
        let dataBaseIndex = DataBaseIndex(objectId: "id", blockDataIndex: 1, inBlockIndex: 2)
        XCTAssertNotNil(dataBaseIndex)
        XCTAssertEqual(dataBaseIndex.objectId, "id")
        XCTAssertEqual(dataBaseIndex.blockDataIndex, 1)
        XCTAssertEqual(dataBaseIndex.inBlockIndex, 2)
    }
}
