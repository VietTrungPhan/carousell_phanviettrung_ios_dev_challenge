//
//  DataBaseTest.swift
//  PostList
//
//  Created by TrungPhan on 8/23/17.
//
//

import XCTest
@testable import PostList
class DataBaseTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    func testInitDataBase() {
        let dataBase = DataBase<Int>()
        XCTAssertNotNil(dataBase)
        XCTAssertNotNil(dataBase.blocks)
        XCTAssertTrue(dataBase.blocks.count == 0)
    }
//    func testInitDataBaseWithBlocks() {
//        var blockDatas: [BlockData<Int>] = []
//        blockDatas[1] = BlockData(items: [1])
//        blockDatas[2] = BlockData(items: [2])
//        let dataBase = DataBase(blocks: blockDatas)
//        XCTAssertNotNil(dataBase)
//        XCTAssertNotNil(dataBase.blocks)
//    }
//    func testSelectAtIndex() {
//        var blockDatas: [BlockData<Int>] = []
//        blockDatas[1] = BlockData<Int>(items: [1])
//        let dataBase = DataBase<Int>(blocks: blockDatas)
//        let compareBlock =  dataBase.Select(byKey: 1)
//        if let object: Int = compareBlock?.Select(atIndex: 0) {
//            XCTAssertEqual(object, 1)
//        }else{
//            assertionFailure()
//        }
//        
//    }

    func testInsert(){
        let dataBase = DataBase<Int>()
        //case create new block and add to DB
        XCTAssertTrue(dataBase.Insert(block: BlockData(items: [1]), forKey: 0))
        XCTAssertEqual(dataBase.Select(byKey: 0)?.Select(atIndex: 0), 1)
        
        //case block existed in DB => append
        XCTAssertTrue(dataBase.Insert(block: BlockData(items: [2]), forKey: 0))
        XCTAssertEqual(dataBase.Select(byKey: 0)?.Select(atIndex: 1), 2)
    }
}

//Test DataBaseIndex
extension DataBaseTest {
    func testInsertDataBaseIndexSuccess(){
        let dataBase = DataBase<Int>()
        XCTAssertTrue(dataBase.insertDataBaseIndex(object: DataBaseIndex(objectId: "id", blockDataIndex: 0, inBlockIndex: 0)))
    }
    func testInsertDataBaseIndexFailed(){
        let dataBase = DataBase<Int>()
        XCTAssertTrue(dataBase.insertDataBaseIndex(object: DataBaseIndex(objectId: "id", blockDataIndex: 0, inBlockIndex: 0)))
        XCTAssertFalse(dataBase.insertDataBaseIndex(object: DataBaseIndex(objectId: "id", blockDataIndex: 0, inBlockIndex: 1)))
    }
    func testGetDataBaseIndex() {
        let dataBase = DataBase<Int>()
        XCTAssertTrue(dataBase.insertDataBaseIndex(object: DataBaseIndex(objectId: "id", blockDataIndex: 0, inBlockIndex: 1)))
        let databaseIndex = dataBase.getDataBaseIndex(byId:"id")
        XCTAssertNotNil(databaseIndex)
        XCTAssertEqual(databaseIndex?.objectId, "id")
        XCTAssertEqual(databaseIndex?.blockDataIndex, 0)
        XCTAssertEqual(databaseIndex?.inBlockIndex, 1)


    }
    func testRemoveDataBaseIndex() {
        let dataBase = DataBase<Int>()
        XCTAssertTrue(dataBase.insertDataBaseIndex(object: DataBaseIndex(objectId: "id", blockDataIndex: 0, inBlockIndex: 1)))
        
        XCTAssertNotNil(dataBase.getDataBaseIndex(byId: "id"))
        
        let _ = dataBase.removeDataBaseIndex(byId: "id")
        XCTAssertNil(dataBase.getDataBaseIndex(byId: "id"))
    }
//    func testUpdateDataBaseIndexSuccess() {
//        let dataBase = DataBase<Int>()
//        XCTAssertTrue(dataBase.insertDataBaseIndex(object: DataBaseIndex(objectId: "id", blockDataIndex: 0, inBlockIndex: 1)))
//        let _ = dataBase.updateDataBaseIndex(objectId: "id", blockDataIndex: 10, inBlockIndex: 20)
//        let dataBaseIndex = dataBase.getDataBaseIndex(byId: "id")
//        XCTAssertEqual(dataBaseIndex?.objectId, "id")
//        XCTAssertEqual(dataBaseIndex?.blockDataIndex, 10)
//        XCTAssertEqual(dataBaseIndex?.inBlockIndex, 20)
//        
//        let _ = dataBase.updateDataBaseIndex(objectId: "id", blockDataIndex: 30, inBlockIndex: nil)
//        XCTAssertEqual(dataBaseIndex?.objectId, "id")
//        XCTAssertEqual(dataBaseIndex?.blockDataIndex, 30)
//        XCTAssertEqual(dataBaseIndex?.inBlockIndex, 20)
//        
//        let _ = dataBase.updateDataBaseIndex(objectId: "id", blockDataIndex: nil, inBlockIndex: 50)
//        XCTAssertEqual(dataBaseIndex?.objectId, "id")
//        XCTAssertEqual(dataBaseIndex?.blockDataIndex, 30)
//        XCTAssertEqual(dataBaseIndex?.inBlockIndex, 50)
//    }
//    
    func testInsertNewObject() {
        let dataBase = DataBase<Int>()
        let isSuccess =  dataBase.Insert(object: 1, byKey: 0)
        XCTAssertTrue(isSuccess)
        let data = dataBase.Select(byKey: 0)?.SelectAll()?.first
        XCTAssertEqual(data, 1)
    }
    
//    func testUpdateObjecByGivingCondition() {
//        let dataBase = DataBase<Int>()
//        let isSuccessInsert =  dataBase.Insert(object: 1, byKey: 0)
//        XCTAssertTrue(isSuccessInsert)
//        
//        let isSuccessUpdate = dataBase.Update(object: 2, byKey: 0) { stored -> Bool in
//            stored == 1
//        }
//        
//        XCTAssertTrue(isSuccessUpdate)
//        let data = dataBase.Select(byKey: 0)?.SelectAll()?.first
//        XCTAssertEqual(data, 2)
//    }
    
}
