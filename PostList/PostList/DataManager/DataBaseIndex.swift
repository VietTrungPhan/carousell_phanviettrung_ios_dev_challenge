//
//  DataBaseIndex.swift
//  PostList
//
//  Created by TrungPhan on 8/24/17.
//
//

/// . DataBaseIndex use to index location of object in data base for fast lookup
/// . In feature this may help Index object title, description... for human friendly search object
protocol DataBaseIndexQuery {
    func getDataBaseIndex(byId: String) -> DataBaseIndex?
    func removeDataBaseIndex(byId: String) -> Bool
    //func updateDataBaseIndex(objectId:String, blockDataIndex: Int?, inBlockIndex: Int?) -> Bool
    func insertDataBaseIndex(object: DataBaseIndex) -> Bool
}

class DataBaseIndex {
    private(set) var objectId: String?
    var blockDataIndex: Int?
    var inBlockIndex: Int?
    
    convenience init(objectId: String, blockDataIndex: Int, inBlockIndex: Int) {
        self.init()
        self.objectId = objectId
        self.blockDataIndex = blockDataIndex
        self.inBlockIndex = inBlockIndex
    }
}
