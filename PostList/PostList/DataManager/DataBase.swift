//
//  DataBase.swift
//  PostList
//
//  Created by TrungPhan on 8/23/17.
//
//
///. DataBase is Class using to store custom Data Objects
///. Object will add to DataBase and group by Index
///. Each index point to  BlockData to optimazation read, write, search.. object(s) later.

import Foundation
protocol DataBaseQuery {
    associatedtype T
    func Select(byKey: Int) -> BlockData<T>?
    func Insert(block: BlockData<T>, forKey:Int) -> Bool
}

class DataBase<T> {
    //Dictionary is key-value type, hashable => fast search by index
    fileprivate var objectsIndexByVotes:[DataBaseIndex] = [] {
        //Just Debug Helper
        //You can filter debug area by keyword "Debug" to have a pretty view ^^!!

        didSet {
          print(objectsIndexByVotes.reduce("") { (result, dataBaseIndex) in
                return result + "Debug: Object:\(dataBaseIndex.objectId!) stored in BlockData byKey:\(dataBaseIndex.blockDataIndex ?? -1000) at Location:\(dataBaseIndex.inBlockIndex ?? -1000) in BlockData\n"
            } + "\\-------------End Log")
        }
    }
    var blocks: [BlockData<T>]
    init() {
        blocks = []
    }
}
extension DataBase {
    convenience init(blocks:[BlockData<T>]) {
        self.init()
        self.blocks = blocks
    }
}

extension DataBase:DataBaseQuery {

    /**
     Insert is func use to add  block to DataBase. If block for key existed we append block || write new block with giving key. In real project should return success callback because of delay by write data operation to ROM memory/HDD/SSD
     
     - parameter block: BlockData.
     - parameter forKey: the key use to point to related BlockData.
     
     - returns: true.
     */
    func Insert(block: BlockData<T>, forKey: Int) -> Bool {
        if let existedBlock = getBlock(byKey: forKey) {
            storeBlock(block: existedBlock + block, forKey: forKey)
        }else {
            storeBlock(block: block, forKey: forKey)
        }
        return true
    }
    /**
     Select is func use to get BlockData by key
     
     - parameter btyKey: Int.
     - returns: BlockData if existed, NULL is not existed.
     */
    func Select(byKey: Int) -> BlockData<T>? {
        if let block = getBlock(byKey: byKey){
            return block
        }
        return nil
    }
    
    /**
     Insert object<T> by key
     
     - parameter object: <T> object to store.
     - parameter byKey: Int key referent to stored object.
     
     - returns: isSuccess.
     */
    func Insert(object:T, byKey: Int) -> Bool {
        return Insert(block: BlockData(items:[object]), forKey: byKey)
    }
    
    /**
     Update object by key and giving condition
     
     - parameter object: <T>.
     - parameter btKey: Int.
     - parameter condition: condition for generic type.
     
     - returns: isSuccess.
     */
    func Update(object:T, byKey: Int, condition: (T)-> Bool) -> Bool {
        if let blockData = Select(byKey: byKey) {
           return blockData.Update(object: object, atIndex: blockData.Index(condition: { object -> Bool in
                return condition(object)
            })!)
        }else {
            return false
        }
    }
}

extension DataBase: DataBaseIndexQuery {

    func getDataBaseIndex(byId: String) -> DataBaseIndex? {
        let filtereds = objectsIndexByVotes.filter{$0.objectId == byId}
        if filtereds.count > 0 {
            return filtereds.first
        }else {
            return nil
        }
    }
    
    func removeDataBaseIndex(byId: String) -> Bool {
        guard let removeObjectIndex = objectsIndexByVotes.index(where: { $0.objectId == byId}) else {return false}
        objectsIndexByVotes.remove(at: removeObjectIndex)
        return true
    }
    

    func insertDataBaseIndex(object: DataBaseIndex) -> Bool {
        //If existed same object ID => reject || Append
        let existedObject = objectsIndexByVotes.contains(where: {$0.objectId == object.objectId})
        if existedObject == false {
            objectsIndexByVotes.append(object)
            return true
        }
        return false
    }
    func getDataBaseIndexedSortedByVoted() -> [DataBaseIndex] {
        return objectsIndexByVotes.sorted { lhs, rhs -> Bool in
            return lhs.blockDataIndex! < rhs.blockDataIndex!
        }
    }
}
extension DataBase {
    
    //We encode/decode and save to local stored (UserDefault for simple case)
    //The <T> object should conform to NSCoding
    func storeBlock(block:BlockData<T>?, forKey:Int) {
        let key = "\(forKey)"
        if let block = block {
            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: block)
            UserDefaults.standard.set(encodedData, forKey: key)
        }else {
            UserDefaults.standard.removeObject(forKey: key)
        }
        UserDefaults.standard.synchronize()
    }
    func getBlock(byKey:Int) -> BlockData<T>? {
        let key = "\(byKey)"
        let decoded  = UserDefaults.standard.object(forKey: key) as? Data
        if let decoded = decoded {
            return NSKeyedUnarchiver.unarchiveObject(with: decoded) as? BlockData<T>
        }
        return nil
    }
}
