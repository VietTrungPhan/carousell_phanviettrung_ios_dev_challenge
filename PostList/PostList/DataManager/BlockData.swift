//
//  File.swift
//  PostList
//
//  Created by TrungPhan on 8/23/17.
//
//
/// . BlockData use to Group stored Object by Index
/// . In this app, we need Sort, Get top 20 posts by number of vote => We use number of vote as Index for optimazation read data from database
import Foundation

protocol BlockDataQuery  {
    associatedtype T
    func Select(atIndex:Int) -> T?
    func SelectAll() -> [T]?
    func Insert(object: T) -> Bool
    func Update(object:T, atIndex:Int) -> Bool
    func Find(byCondition:(T)->Bool) -> [T]
}

class BlockData<T>: NSObject, NSCoding {
    fileprivate var items:[T]
    init(items:[T]) {
        self.items = items
    }
    required convenience init(coder aDecoder: NSCoder) {
        let items = aDecoder.decodeObject(forKey: "items") as! [T]
        self.init(items: items)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(items, forKey: "items")
    }
}
extension BlockData:BlockDataQuery {
    /**
     SelectAll is function return all stored object(s)
    - returns: Array of object <T>.
     */
    func SelectAll() -> [T]? {
        return items
    }
    
    /**
     Select is function return object at index
     
     - parameter atIndex: Int index to receipt object.
     - returns: <T> if selectedIndext not out of range.
     */
    func Select(atIndex:Int) -> T? {
        if atIndex < items.count  {
            return items[atIndex]
        }else {
            return nil
        }
    }
    
    /**
     Insert use to add new object to BlockData
     
     - parameter object: <T> new object to add.
     
     - returns: true when success add.
     */
    func Insert(object: T) -> Bool {
        items.append(object)
        return true
    }
    
    /**
     Update is function use to update object by giving new object and indext to update
     
     - parameter object: <T>.
     - parameter atIndex: Int index use to update.
     
     - returns: true if index not out of range.
     */
    func Update(object:T, atIndex:Int) -> Bool{
        if atIndex < items.count {
            items[atIndex] = object
            return true
        }else {
            return false
        }
    }
    /**
     Find is function  use to fillter saved object(s) by giving condition
     
     - parameter condition: Closure use to give condation later (because web don't know object type yet).
     - returns: Array of object match condition.
     */
    func Find(byCondition condition:(T) -> Bool) -> [T] {
        return items.filter({ (item: T) in
            return condition(item)
        })
    }
    
    /**
     Index is func return Index of object in Blockdata
     
     - parameter condition: condition ob return object Index.
     - returns: Int? index.
     */
    func Index(condition:(T)->Bool) -> Int? {
        return items.index(where: { item -> Bool in
            return condition(item)
        })
    }
    
    /**
     + is function thay merge 2 BlockData objects
     
     - parameter lhs: BlockData.
     - parameter rhs: BlockData.
     
     - returns: new BlockData.
     */
    static func +(lhs:BlockData, rhs:BlockData) -> BlockData {
        return BlockData(items:lhs.items + rhs.items)
    }

}
