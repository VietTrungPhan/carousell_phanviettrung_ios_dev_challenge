//
//  DataManagerTest.swift
//  PostList
//
//  Created by TrungPhan on 8/25/17.
//
//

import XCTest
@testable import PostList
class DataManagerTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testInsertNewPost() {
        let dataManager = DataManager()
        let post = PostModel(title: "Carousell", detail: "Try to test Nick", byUser: "Zelda")
        let isSuccess = dataManager.addnewPost(post: post)
        
        
        XCTAssertTrue(isSuccess)
        
        let indexed = dataManager.database.getDataBaseIndex(byId: post.id)
        XCTAssertNotNil(indexed)
        XCTAssertEqual(indexed?.blockDataIndex, 0)
        XCTAssertEqual(indexed?.inBlockIndex, 0)
        
        
        let post1 = PostModel(title: "Nick", detail: "Try to pass challenge", byUser: "Nick")
        post1.votes = 0
        let _ = dataManager.addnewPost(post: post1)

    }
    
//    func testUpDatePostInSameBlockData() {
//        let dataManager = DataManager()
//        let post = PostModel(title: "Carousell", detail: "Try to test Nick", byUser: "Zelda")
//        let _ = dataManager.addnewPost(post: post)
//        XCTAssertEqual(dataManager.database.Select(byKey: 0)?.Select(atIndex: 0)?.detail, "Try to test Nick")
//        
//        post.detail = "Hey, Nick should failue our test, he is don't know how to write code"
//        let _ = dataManager.updatePost(post: post)
//        XCTAssertEqual(dataManager.database.Select(byKey: 0)?.Select(atIndex: 0)?.detail, "Hey, Nick should failue our test, he is don't know how to write code")
//
//    }
//    func testUpDatePostInDiffirentBlockData() {
//        let dataManager = DataManager()
//        let post = PostModel(title: "Carousell", detail: "Try to test Nick", byUser: "Zelda")
//        let _ = dataManager.addnewPost(post: post)
//        XCTAssertEqual(dataManager.database.Select(byKey: 0)?.Select(atIndex: 0)?.detail, "Try to test Nick")
//        
//        post.detail = "Hey, Nick should failue our test, he is don't know how to write code"
//        post.votes = 1
//        let _ = dataManager.updatePost(post: post)
//        XCTAssertEqual(dataManager.database.Select(byKey: 1)?.Select(atIndex: 0)?.detail, "Hey, Nick should failue our test, he is don't know how to write code")        
//    }
    func testGetTopPostFromDBAsscending() {
        let dataManager = DataManager()
        for i in 1...30 {
            let postModel = PostModel(title: "test\(i)", detail: "", byUser: "")
            postModel.votes = i
            let _ = dataManager.addnewPost(post: postModel)
        }
        let results = dataManager.getTopPosts(isAsscending: true, numberOfItems: 20)
        XCTAssertEqual(results?.count, 20)
        XCTAssertEqual(results?.first?.votes, 30)
        XCTAssertEqual(results?.last?.votes, 11)

    }
    func testGetTopPostFromDBDecending() {
        let dataManager = DataManager()
        for i in 1..<30 {
            let postModel = PostModel(title: "test\(i)", detail: "", byUser: "")
            postModel.votes = i
            let _ = dataManager.addnewPost(post: postModel)
        }
        let results = dataManager.getTopPosts(isAsscending: false, numberOfItems: 20)
        XCTAssertEqual(results?.count, 20)
        XCTAssertEqual(results?.first?.votes, 1)
        XCTAssertEqual(results?.last?.votes, 20)
        
    }
    func testGetTopPostZero() {
        let dataManager = DataManager()
        let results = dataManager.getTopPosts(isAsscending: true, numberOfItems: 20)
        XCTAssertNil(results)
    }
    
}
