//
//  NSMutableAttributeString+Extension.swift
//  PostList
//
//  Created by TrungPhan on 8/28/17.
//
//

import Foundation
import UIKit
extension NSMutableAttributedString {
    @discardableResult func appEndColor(text:String, color: UIColor, fontSize: CGFloat) -> NSMutableAttributedString {
        let attrs:[String:AnyObject] = [NSFontAttributeName : UIFont.systemFont(ofSize: fontSize), NSForegroundColorAttributeName: color]
        let coloredString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(coloredString)
        return self
    }
}
